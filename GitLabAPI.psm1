# GitLabAPI.psm1

function Get-GitLabProjectId {
    [CmdletBinding()]
    Param(
        [Parameter(Mandatory=$true)]
        [string]$ProjectName,

        [Parameter(Mandatory=$true)]
        [string]$GitLabToken,

        [string]$GitLabURL = 'https://gitlab.com'
    )

    $encodedProjectName = [System.Web.HttpUtility]::UrlEncode($ProjectName)
    $url = "$GitLabURL/api/v4/projects?search=$encodedProjectName"
    
    $headers = @{
        "PRIVATE-TOKEN" = $GitLabToken
    }

    try {
        $response = Invoke-RestMethod -Uri $url -Method Get -Headers $headers
        $project = $response | Where-Object { $_.name -eq $ProjectName }
        if ($project) {
            return $project.id
        } else {
            Write-Error "Project '$ProjectName' not found."
        }
    }
    catch {
        Write-Error $_.Exception.Message
    }
}

function Set-GitLabProjectDefaults {
    [CmdletBinding()]
    Param(
        [Parameter(Mandatory=$true)]
        [string]$GitLabToken,

        [Parameter(Mandatory=$true)]
        [int]$ProjectId,

        [string]$GitLabURL = 'https://gitlab.com'# Change this if you have a self-hosted instance

    )

    $url = "$GitLabURL/api/v4/projects/$ProjectId"
    
    $body = @{
        description = 'Sam Thota desc'
        # default_branch = $(if ($default_branch) { 'integration' } else { 'main' })
        issues_access_level = 'disabled'
        forking_access_level = 'disabled'
        feature_flags_access_level = 'disabled'
        model_experiments_access_level = 'disabled'
        model_registry_access_level = 'disabled'
        packages_enabled = $False
        requirements_access_level = 'disabled'
        lfs_enabled = $False
        wiki_access_level = 'disabled'
        builds_access_level = 'disabled'
        snippets_access_level = 'disabled'
        pages_access_level = 'disabled'
        analytics_access_level = 'disabled'
        container_registry_access_level = 'disabled'
        security_and_compliance_access_level = 'disabled'
        monitor_access_level = 'disabled'
        environments_access_level = 'disabled'
        releases_access_level = 'disabled'
        infrastructure_access_level = 'disabled'
        remove_source_branch_after_merge = $False
        squash_option = 'never'
        only_allow_merge_if_all_discussions_are_resolved = $True
    } | ConvertTo-Json

    $headers = @{
        "PRIVATE-TOKEN" = $GitLabToken
        "Content-Type" = "application/json"
    }

    try {
        $response = Invoke-RestMethod -Uri $url -Method Put -Headers $headers -Body $body
        Write-Host "Project settings updated successfully."
    }
    catch {
        Write-Error $_.Exception.Message
    }
}